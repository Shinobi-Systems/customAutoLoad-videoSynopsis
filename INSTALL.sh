SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH
apt install imagemagick -y
npm install shinobi-video-synopsis --unsafe-perm --prefix "$SCRIPTPATH"
