const { Worker } = require('shinobi-worker')
const spawn = require('child_process').spawn
const fs = require('fs-extra')
module.exports = function(s,config,lang,app,io){
    const createMainDirectory = () => {
        s.dir.videoSynopsisBuild = config.videoSynopsisBuild ? config.videoSynopsisBuild : config.shmDir + 'videoSynopsisBuild/'
        s.dir.videoSynopsisBuild = s.checkCorrectPathEnding(s.dir.videoSynopsisBuild)
        if(!fs.existsSync(s.dir.videoSynopsisBuild)){
            fs.mkdirSync(s.dir.videoSynopsisBuild);
        }
    }
    const getVideoSynopsisBuildDirectory = (monitor) => {
        return s.dir.videoSynopsisBuild + monitor.ke + '/' + monitor.mid + '/'
    }
    const getDateParts = (currentDate) => {
        return {
            year: currentDate.getFullYear(),
            month: currentDate.getMonth(),
            day: currentDate.getDate(),
        }
    }
    const pendingCallbacks = {}
    const runWorkerFunction = (worker,options) => {
        return new Promise((resolve,reject) => {
            const callbackId = s.gid(6)
            options.callbackId = callbackId
            worker.postMessage(options)
            pendingCallbacks[options.callbackId] = (response) => {
                resolve(response)
            }
        })
    }
    const getVideoFilesBetweenTimes = (req,user) => {
        //these files are assumed to be MP4 files.
        return new Promise((resolve,reject) => {
            const processVideos = (videos) => {
                var videoFiles = []
                videos.forEach((video) => {
                    const filepath = s.getVideoDirectory(video) + s.formattedTime(video.time)//.mp4
                    try{
                        if(fs.lstatSync(filepath + '.mp4').size){
                            videoFiles.push(filepath)
                        }
                    }catch(err){
                        s.debugLog(err)
                    }
                })
                resolve(videoFiles)
            }
            if(req.body.videos){
                processVideos(req.body.videos)
            }else{
                const userDetails = user.details
                s.sqlQueryBetweenTimesWithPermissions({
                    table: 'Videos',
                    user: user,
                    noCount: true,
                    groupKey: req.params.ke,
                    monitorId: req.params.id,
                    startTime: req.query.start,
                    endTime: req.query.end,
                    startTimeOperator: req.query.startOperator,
                    endTimeOperator: req.query.endOperator,
                    limit: req.query.limit,
                    archived: req.query.archived,
                    endIsStartTo: !!req.query.endIsStartTo,
                    parseRowDetails: false,
                    rowName: 'videos',
                    preliminaryValidationFailed: (
                        user.permissions.watch_videos === "0" ||
                        (!userDetails.video_view || userDetails.video_view.indexOf(monitorId)===-1)
                    )
                },(response) => {
                    if(response.videos){
                        processVideos(response.videos)
                    }else{
                        resolve([])
                    }
                })
            }
        })
    }
    const handleWorkerResponse = (monitor,activeMonitor,data) => {
        if(data.callbackId && pendingCallbacks[data.callbackId]){
            pendingCallbacks[data.callbackId](data.response)
            pendingCallbacks[data.callbackId] = undefined
            delete(pendingCallbacks[data.callbackId])
        }
        switch(data.f){
            case'video_synopsis_complete':
                if(!data.cancelled){
                    s.insertFileBinEntry({
                        ke: monitor.ke,
                        mid: monitor.mid,
                        name: data.filename,
                        size: data.fileSize,
                        details: data.dateParts,
                    })
                    s.setDiskUsedForGroup(data.ke, data.fileSize / 1048576, 'fileBin')
                }
                s.tx(data,'GRP_' + data.ke)
                try{
                    delete(activeMonitor.worker)
                }catch(err){
                    s.debugLog(err)
                }
                delete(activeMonitor.worker)
            break;
            case'video_synopsis_started':
            case'video_synopsis_percent':
            case'video_synopsis_log':
                s.tx(data,'GRP_' + monitor.ke)
            break;
            case'newEngine':
                activeMonitor.videoSynopsis = data.engine
                existingEngine = data.engine
            break;
        }
    }
    const createNewProcess = (monitor) => {
        const activeMonitor = s.group[monitor.ke].activeMonitors[monitor.mid]
        const workerProcess = Worker(
            __dirname + '/newThread.js',
            {
                json: true,
                debug: true,
            }
        )
        workerProcess.on('message',function(data){
            console.log('message FROM WORKER',data)
            handleWorkerResponse(monitor,activeMonitor,data)
        })
        workerProcess.on('close',function(){
            activeMonitor.worker = null
        })
        // only work when ``"debug": true` is applied to Worker options.
        workerProcess.on('error',function(data){
            s.debugLog(data)
        })
        workerProcess.on('failedParse',function(stringThatFailedToParse){
            s.debugLog(stringThatFailedToParse)
        })
        return workerProcess
    }
    createMainDirectory()
    /**
    * Page : Start Video Synopsis Build
     */
    app.post(config.webPaths.apiPrefix+':auth/videoSynopsis/:ke/:id', async (req,res) => {
        const currentDate = new Date()
        const { year, month, day } = getDateParts(currentDate)
        const streamDirectory =  getVideoSynopsisBuildDirectory({mid: req.params.id, ke: req.params.ke})
        const fileBinDirectory =  s.getFileBinDirectory({mid: req.params.id, ke: req.params.ke}) + year + '/' + month + '/' + day + '/'
        if(!fs.existsSync(streamDirectory)){
            fs.mkdirSync(streamDirectory,{recursive: true});
        }
        if(!fs.existsSync(fileBinDirectory)){
            fs.mkdirSync(fileBinDirectory,{recursive: true});
        }
        s.auth(req.params, async (user) => {
            const newFileName = s.formattedTime(currentDate) + '-synopsis.mp4'
            const videoPaths = await getVideoFilesBetweenTimes(req,user)
            const activeMonitor = s.group[req.params.ke].activeMonitors[req.params.id]
            var existingEngine = activeMonitor.videoSynopsis
            var worker = activeMonitor.worker
            if(!worker){
                worker = createNewProcess({
                    ke: req.params.ke,
                    mid: req.params.id,
                })
                activeMonitor.worker = worker
                await runWorkerFunction(worker,{
                    function: 'createMonitorVideoSynopsisEngine',
                    arguments: [
                        {mid: req.params.id, ke: req.params.ke},
                        streamDirectory,
                        config.debugLog,
                        config.ffmpegDir
                    ],
                })
                runWorkerFunction(worker,{
                    function: 'runVideoSynopsis',
                    arguments: [{
                        streamDirectory: streamDirectory,
                        videos: videoPaths,
                        output: fileBinDirectory + newFileName,
                        dateParts: getDateParts(currentDate)
                    }],
                })
            }
            s.closeJsonResponse(res,{
                ok: true,
                msg: existingEngine && existingEngine.runningProcesses ? lang['Already Processing'] : null
            })
        },res,req);
    })
    /**
    * Page : Start Video Synopsis Cancel
     */
    app.get(config.webPaths.apiPrefix+':auth/videoSynopsis/:ke/:id/cancel', async (req,res) => {
        s.auth(req.params, async (user) => {
            const activeMonitor = s.group[req.params.ke].activeMonitors[req.params.id]
            const worker = activeMonitor.worker
            if(worker){
                await runWorkerFunction(worker,{
                    function: 'cancel',
                })
                try{
                    activeMonitor.worker.kill('SIGTERM')
                    activeMonitor.worker = null
                }catch(err){
                    s.debugLog(err)
                }
            }
            s.closeJsonResponse(res,{
                ok: true,
            })
        },res,req);
    })
    /**
    * Page : Start Video Synopsis Info
     */
    app.get(config.webPaths.apiPrefix+':auth/videoSynopsisAbout/:ke/:id', async (req,res) => {
        s.auth(req.params, async (user) => {
            const activeMonitor = s.group[req.params.ke].activeMonitors[req.params.id]
            const worker = activeMonitor.worker
            s.closeJsonResponse(res,{
                ok: true,
                about: worker ? await runWorkerFunction(worker,{
                    function: 'about',
                }) : null
            })
        },res,req);
    })
}
