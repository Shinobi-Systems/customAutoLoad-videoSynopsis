var videoSynopsisWindow = $('#videoSynopsis')
var videoSynopsisSearchForm = $('#videoSynopsisSearchForm')
var videoSynopsisSearchDateRange = $('#videoSynopsis_daterange')
var videoSynopsisMonitorsSelector = $('#videoSynopsis_monitors')
var videoSynopsisVideoPreview = $('#videoSynopsis_preview')
var videoSynopsisVideoLogs = $('#videoSynopsis_logs')
var videoSynopsisVideoStatus = $('#videoSynopsis_status tbody')
var videoSynopsisVideoListing = videoSynopsisWindow.find('.video-files')
var videoSynopsisFileBinListing = videoSynopsisWindow.find('.fileBin-files')
var selectedMonitorId;
var loadedVideos = {}
var loadedFileBinVideos = {}
var selectedVideos = {}
var drawLogRow = function(log){
    videoSynopsisVideoLogs.append(`<pre class="monospace">${log[0] instanceof Object ? JSON.stringify(log[0],null,3) : log.join(' ')}</pre>`)
}
var initializeElements = function(){
    var buttonData = `<li class="mdl-menu__item mdl-js-ripple-effect" data-toggle="modal" data-target="#videoSynopsis">
        <div>
            <i class="fa fa-film"></i>
            <div>${lang['Video Synopsis']}</div>
        </div>
        <span class="mdl-menu__item-ripple-container">
            <span class="mdl-ripple"></span>
        </span>
    </li>`
    $('#accbtn').next().prepend(buttonData)
}
var showBuildButton = function(show){
    videoSynopsisWindow.find('.begin-video-build')[show ? 'show' : 'hide']()
    videoSynopsisWindow.find('.cancel-video-build')[show ? 'hide' : 'show']()
}
var writeStatusTable = function(monitorId,about){
    showBuildButton(!about || about.cancelledProcess)
    videoSynopsisVideoStatus.html(about ? `
        <tr>
            <td>${lang['Monitor ID']}</td>
            <td>${monitorId}</td>
        </tr>
        <tr>
            <td>${lang['Currently Running']}</td>
            <td class="runningProcesses">${about.runningProcesses ? 'Yes' : 'No'}</td>
        </tr>
        <tr>
            <td>${lang['Completed']}</td>
            <td><span class="percentCompleted">${about.percentCompleted}</span>%</td>
        </tr>
        <tr>
            <td>${lang['Started at']}</td>
            <td>${moment(about.initiatedAt).format()}</td>
        </tr>
        <tr>
            <td>${lang['Cancelled']}</td>
            <td class="cancelledProcess">${about.cancelledProcess ? 'Yes' : 'No'}</td>
        </tr>
    ` : `<tr><td>${lang['No Engine Running']}</td></tr>`)
}
var drawFileBinList = function(files){
    var html = ''
    $.each(files,function(n,video){
        loadedFileBinVideos[video.name] = video
        html += `<tr ke="${video.ke}" mid="${video.mid}" filename="${video.name}">
            <td>
                <div><h5>${video.mid}</h5></div>
                <div><h5>${video.time}</h5></div>
                <div><small>${video.name}</small></div>
            </td>
            <td>
                <a status="${video.status}" preview class="btn btn-sm btn-info"><i class="fa fa-play"></i></a>
            </td>
            <td class="text-right">
                <li class="mdl-list__item" style="display: inline-block;">
                    <span class="mdl-list__item-secondary-action">
                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                            <input type="checkbox" value="${video.filename}" class="video-toggle mdl-switch__input"/>
                        </label>
                    </span>
                </li>
            </td>
        </tr>`
    })
    videoSynopsisFileBinListing.html(html)
    componentHandler.upgradeAllRegistered()
}
var drawVideosList = function(videos){
    videoSynopsisVideoListing.empty()
    if(videos){
        $.each(videos,function(n,video){
            loadedVideos[video.filename] = video
            videoSynopsisVideoListing.append(`<tr ke="${video.ke}" mid="${video.mid}" filename="${video.filename}">
                <td>
                    <div><h5>${video.mid}</h5></div>
                    <div><h5>${video.time}</h5></div>
                    <div><small>${video.end}</small></div>
                </td>
                <td>
                    <a status="${video.status}" preview class="btn btn-sm btn-info"><i class="fa fa-play"></i></a>
                </td>
                <td class="text-right">
                    <li class="mdl-list__item" style="display: inline-block;">
                        <span class="mdl-list__item-secondary-action">
                            <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect">
                                <input type="checkbox" value="${video.filename}" class="video-toggle mdl-switch__input"/>
                            </label>
                        </span>
                    </li>
                </td>
            </tr>`)
        })
    }
    componentHandler.upgradeAllRegistered()
}
var getVideosForSelection = function(){
    videoSynopsisVideoLogs.empty()
    var monitorId = videoSynopsisMonitorsSelector.val();
    var time = videoSynopsisSearchDateRange.data('daterangepicker');
    selectedVideos = {}
    loadedFileBinVideos = {}
    loadedVideos = {}
    selectedMonitorId = `${monitorId}`
    $.getJSON(getApiPrefix('videos') + `/${monitorId}?&start=`+time.startDate.utc().format('YYYY-MM-DDTHH:mm:ss')+`&end=`+time.endDate.utc().format('YYYY-MM-DDTHH:mm:ss'),function(data){
        drawVideosList(data.videos)
    })
    $.getJSON(getApiPrefix('videoSynopsisAbout') + `/${monitorId}`,function(data){
        var about = data.about
        writeStatusTable(monitorId,about);
        $.each(about.logs,function(n,log){
            drawLogRow(log)
        })
    })
    $.getJSON(getApiPrefix('fileBin') + `/${monitorId}`,function(data){
        drawFileBinList(data.files)
    })
}
var previewVideo = function(video){
    var url = $.ccio.init('videoUrlBuild',video)
    videoSynopsisVideoPreview.html(`<video class="video_video" video="${url}" preload controls autoplay><source src="${url}" type="video/mp4"></video>`)
}
var getSelectedVideos = function(){
    return Object.values(selectedVideos).sort(function (a, b) {
        return new Date(a.time) > new Date(b.time);
    })
}
var beginVideoBuild = function(callback){
    var monitorId = videoSynopsisMonitorsSelector.val();
    $.post(getApiPrefix('videoSynopsis') + `/${monitorId}`,{
        videos: getSelectedVideos()
    },callback)
}
var cancelVideoBuild = function(callback){
    var monitorId = videoSynopsisMonitorsSelector.val();
    $.get(getApiPrefix('videoSynopsis') + `/${monitorId}/cancel`,callback)
}
videoSynopsisWindow.on('click','[preview]',function(){
    var el = $(this)
    var filename = el.parents('[filename]').attr('filename')
    previewVideo(loadedVideos[filename] || loadedFileBinVideos[filename])
})
videoSynopsisWindow.on('click','.begin-video-build',function(){
    beginVideoBuild()
})
videoSynopsisWindow.on('click','.cancel-video-build',function(){
    cancelVideoBuild()
})
videoSynopsisVideoListing.on('change','.video-toggle',function(){
    var el = $(this)
    var filename = el.parents('[filename]').attr('filename')
    if(el.prop('checked') === true){
        selectedVideos[filename] = loadedVideos[filename]
    }else{
        selectedVideos[filename] = undefined
    }
})
videoSynopsisWindow.on('shown.bs.modal',function(){
    if(videoSynopsisMonitorsSelector.children().length !== Object.keys($.ccio.mon).length){
        videoSynopsisMonitorsSelector.empty()
        $.each($.ccio.mon,function(n,v){
            $.ccio.tm('option',{
                id: v.mid,
                name: v.name,
            },'#videoSynopsis_monitors')
        })
        videoSynopsisMonitorsSelector.val()
        getVideosForSelection()
    }
})
$(document).ready(function(){
    videoSynopsisMonitorsSelector.change(function(){
        getVideosForSelection()
    })
    videoSynopsisSearchDateRange.daterangepicker({
        startDate:$.ccio.timeObject().subtract(moment.duration("24:00:00")),
        endDate:$.ccio.timeObject().add(moment.duration("24:00:00")),
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        timePickerIncrement: 30,
        locale: {
            format: 'DD/MM/YYYY h:mm A'
        }
    },function(start, end, label){
        getVideosForSelection()
    });
    videoSynopsisSearchForm.submit(function(e){
        e.preventDefault()
        getVideosForSelection()
        return false;
    })
    $user.ws.on('f', function(data){
        if(data.mid === selectedMonitorId){
            switch(data.f){
                case'video_synopsis_percent':
                    videoSynopsisVideoStatus.find('.percentCompleted').text(data.percent)
                break;
                case'video_synopsis_started':
                    writeStatusTable(data.mid,data.about)
                break;
                case'video_synopsis_complete':
                    showBuildButton(true)
                    videoSynopsisVideoStatus.find('.runningProcesses').text('No')
                    if(data.cancelled)videoSynopsisVideoStatus.find('.cancelledProcess').text('Yes')
                break;
                case'video_synopsis_log':
                    drawLogRow(data.log)
                break;
            }
        }
    })
    initializeElements()
})
