const fs = require('fs');
const { buildParentPort } = require('shinobi-worker')
const VideoSynopsis = require('shinobi-video-synopsis')
var videoProcessor;


process.send = process.send || function () {};

const getFileNameFromPath = (filePath) => {
    const filePathParts = filePath.split('/')
    return filePathParts[filePathParts.length - 1]
}

const exitAction = function(){
    if(videoProcessor){
        const monitorConfig = videoProcessor.monitorConfig
        postMessageToParent({
            f: 'video_synopsis_complete',
            ke: monitorConfig.ke,
            mid: monitorConfig.mid,
            fileSize: !videoProcessor.cancelledProcess ? fileStats.size : null,
            cancelled: videoProcessor.cancelledProcess
        })
    }
}
process.on('SIGTERM', exitAction);
process.on('SIGINT', exitAction);
process.on('exit', exitAction);

const parentPort = buildParentPort({
    json: true,
    debug: true,
    uncaughtException: true
})
parentPort.on('message',async (data) => {
    var response = {}
    switch(data.function){
        case'createMonitorVideoSynopsisEngine':
            response = await createMonitorVideoSynopsisEngine(...data.arguments)
        break;
        case'runVideoSynopsis':
            response = await runVideoSynopsis(...data.arguments)
        break;
        case'about':
            response = await videoProcessor.about()
        break;
        case'cancel':
            response = await videoProcessor.cancel()
        break;
    }
    if(data.function && data.callbackId){
        postMessageToParent({
            callbackId: data.callbackId,
            response: response,
        })
    }
})
const postMessageToParent = (json) => {
    return parentPort.postMessage(json)
}
const postErrorToParent = function(text){
    return parentPort.postMessage(text)
}
const createMonitorVideoSynopsisEngine = async (monitorConfig,streamDirectory,debugLog,ffmpegDir) => {
    const newEngine = VideoSynopsis.newEngine({
        // debugLog: true,
        // debugLog: debugLog,
        //build directories
        subtractedBackgroundsDirectory: streamDirectory + 'rawSubtractions/',
        mergedFramesDirectory: streamDirectory + 'mergedFrames/',
        firstFrameDirectory: streamDirectory + 'firstFrame/',
        ffmpegBinary: ffmpegDir,
    })
    newEngine.monitorConfig = monitorConfig
    newEngine.std.on('percent',async (percent) => {
        if(percent === 0){
            postMessageToParent({
                f: 'video_synopsis_started',
                ke: monitorConfig.ke,
                mid: monitorConfig.mid,
                about: newEngine ? await newEngine.about() : null
            })
        }
        postMessageToParent({
            f: 'video_synopsis_percent',
            ke: monitorConfig.ke,
            mid: monitorConfig.mid,
            percent: percent
        })
    })
    newEngine.std.on('stderr',(logData) => {
        postMessageToParent({
            f: 'video_synopsis_log',
            ke: monitorConfig.ke,
            mid: monitorConfig.mid,
            log: logData
        })
    })
    postMessageToParent({
        f: 'newEngine',
        engine: await newEngine.about()
    });
    videoProcessor = newEngine
    videoProcessor.monitorConfig = monitorConfig
    return await newEngine.about()
}

const runVideoSynopsis = async (options) => {
    const monitorConfig = videoProcessor.monitorConfig
    const fileOutputPath = options.output || options.streamDirectory + 'videoOutput.mp4'
    videoProcessor.logStderr(options)
    await videoProcessor.run({
        videos: options.videos,
        fps: options.fps || 20,
        output: fileOutputPath,
    });
    await videoProcessor.cleanBuildFiles();
    fs.stat(fileOutputPath,(err,fileStats) => {
        if(err)videoProcessor.logStderr(err);
        postMessageToParent({
            f: 'video_synopsis_complete',
            ke: monitorConfig.ke,
            mid: monitorConfig.mid,
            filename: getFileNameFromPath(fileOutputPath),
            fileSize: !videoProcessor.cancelledProcess ? fileStats.size : null,
            cancelled: videoProcessor.cancelledProcess,
            dateParts: options.dateParts
        })
        setTimeout(() => {
            process.exit()
        },3000)
    })
}
