const fs = require('fs');
const { parentPort } = require('worker_threads');
const VideoSynopsis = require('shinobi-video-synopsis')
var videoProcessor;

parentPort.on('message', async (data) => {
    if(!data)return console.log('no data!');
    console.log('TO WORKER',data);
    var response = {}
    switch(data.function){
        case'createMonitorVideoSynopsisEngine':
            response = await createMonitorVideoSynopsisEngine(...data.arguments)
        break;
        case'runVideoSynopsis':
            response = await runVideoSynopsis(...data.arguments)
        break;
        case'about':
            response = await videoProcessor.about()
        break;
        case'cancel':
            response = await videoProcessor.cancel()
        break;
    }
    if(data.function && data.callbackId){
        parentPort.postMessage({
            callbackId: data.callbackId,
            response: response,
        })
    }
})

const createMonitorVideoSynopsisEngine = async (monitorConfig,streamDirectory,debugLog,ffmpegDir) => {
    console.log(streamDirectory)
    const newEngine = VideoSynopsis.newEngine({
        // debugLog: true,
        debugLog: debugLog,
        //build directories
        subtractedBackgroundsDirectory: streamDirectory + 'rawSubtractions/',
        mergedFramesDirectory: streamDirectory + 'mergedFrames/',
        firstFrameDirectory: streamDirectory + 'firstFrame/',
        ffmpegBinary: ffmpegDir,
    })
    newEngine.monitorConfig = monitorConfig
    newEngine.std.on('percent',async (percent) => {
        if(percent === 0){
            parentPort.postMessage({
                f: 'video_synopsis_started',
                ke: monitorConfig.ke,
                mid: monitorConfig.mid,
                about: newEngine ? await newEngine.about() : null
            })
        }
        parentPort.postMessage({
            f: 'video_synopsis_percent',
            ke: monitorConfig.ke,
            mid: monitorConfig.mid,
            percent: percent
        })
    })
    newEngine.std.on('stderr',(logData) => {
        parentPort.postMessage({
            f: 'video_synopsis_log',
            ke: monitorConfig.ke,
            mid: monitorConfig.mid,
            log: logData
        })
        console.log(logData)
    })
    parentPort.postMessage({
        f: 'newEngine',
        engine: await newEngine.about()
    });
    videoProcessor = newEngine
    return await newEngine.about()
}

const runVideoSynopsis = async (options) => {
    const monitorConfig = videoProcessor.monitorConfig
    const fileOutputPath = options.output || options.streamDirectory + 'videoOutput.mp4'
    await videoProcessor.run({
        videos: options.videos,
        fps: options.fps || 20,
        output: fileOutputPath
    });
    await videoProcessor.cleanBuildFiles();
    if(!videoProcessor.cancelledProcess){
        const fileStats = fs.statSync(fileOutputPath)
    }
    parentPort.postMessage({
        f: 'video_synopsis_complete',
        ke: monitorConfig.ke,
        mid: monitorConfig.mid,
        fileSize: !videoProcessor.cancelledProcess ? fileStats.size : null,
        cancelled: videoProcessor.cancelledProcess
    })
}
